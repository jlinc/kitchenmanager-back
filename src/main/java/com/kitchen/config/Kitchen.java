package com.kitchen.config;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
public class Kitchen {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	private String kitchenName;
	private String kitchenDescription;
	private BigDecimal defaultMenuEntryPrice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKitchenName() {
		return kitchenName;
	}

	public void setKitchenName(String kitchenName) {
		this.kitchenName = kitchenName;
	}

	public String getKitchenDescription() {
		return kitchenDescription;
	}

	public void setKitchenDescription(String kitchenDescription) {
		this.kitchenDescription = kitchenDescription;
	}

	public BigDecimal getDefaultMenuEntryPrice() {
		return defaultMenuEntryPrice;
	}

	public void setDefaultMenuEntryPrice(BigDecimal defaultMenuEntryPrice) {
		this.defaultMenuEntryPrice = defaultMenuEntryPrice;
	}
}
