package com.kitchen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.security.Principal;

@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class})
@EnableSwagger2
@EnableOAuth2Sso
@RestController
public class KitchenManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KitchenManagerApplication.class, args);
	}

	@RequestMapping("/user")
	public Principal user(Principal principal) {
		return principal;
	}


}
