package com.kitchen.error;

import org.omg.CORBA.DynAnyPackage.Invalid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@ControllerAdvice
public class ErrorControllerHandler {

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorResponse notFound(ResourceNotFoundException exception) {
		return new ErrorResponse(exception.getMessage(), exception.getCode());
	}

	@ExceptionHandler(InvalidIdException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResponse invalidId(ResourceNotFoundException exception) {
		return new ErrorResponse(exception.getMessage(), exception.getCode());
	}
}
