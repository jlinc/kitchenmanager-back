package com.kitchen.error;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public class ResourceNotFoundException extends RuntimeException {

	private ErrorCode code;

	public ResourceNotFoundException() {
	}

	public ResourceNotFoundException(String message, ErrorCode code) {
		super(message);
		this.code = code;
	}

	public ErrorCode getCode() {
		return code;
	}

	public void setCode(ErrorCode code) {
		this.code = code;
	}
}
