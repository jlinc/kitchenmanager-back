package com.kitchen.error;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public class ErrorResponse {

	private String message;
	private ErrorCode code;

	public ErrorResponse(String message, ErrorCode code) {
		this.message = message;
		this.code = code;
	}

	public ErrorResponse() {
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public ErrorCode getCode() {
		return code;
	}

	public void setCode(ErrorCode code) {
		this.code = code;
	}
}
