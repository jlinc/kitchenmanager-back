package com.kitchen.error;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public enum ErrorCode {

	KITCHEN_NOT_FOUND,
	ADMIN_NOT_FOUND,
	CONSUMER_NOT_FOUND,
	PLACE_NOT_FOUND,
	MISSING_KITCHEN,
	MISSING_MENU,
	MISSING_FOOD,
	MISSING_MENU_ENTRY,
	INVALID_KITCHEN;

}
