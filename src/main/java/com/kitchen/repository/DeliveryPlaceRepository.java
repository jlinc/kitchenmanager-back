package com.kitchen.repository;

import com.kitchen.model.DeliveryPlace;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Luis Manuel Sala Espiell on 13/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public interface DeliveryPlaceRepository extends CrudRepository<DeliveryPlace, Long> {


	Page<DeliveryPlace> findAll(Pageable page);

}
