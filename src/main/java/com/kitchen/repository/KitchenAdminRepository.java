package com.kitchen.repository;

import com.kitchen.model.KitchenAdmin;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public interface KitchenAdminRepository extends CrudRepository<KitchenAdmin, Long> {
}
