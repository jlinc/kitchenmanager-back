package com.kitchen.repository;

import com.kitchen.model.MenuOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 02/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public interface MenuOrderRepository extends CrudRepository<MenuOrder, Long> {

	List<MenuOrder> findAllByKitchenConsumerId(Long consumerId);

	Long countByDeliveryPlaceId(Long deliveryPlaceId);
}
