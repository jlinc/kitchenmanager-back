package com.kitchen.repository;

import com.kitchen.model.MenuEntry;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Luis Manuel Sala Espiell on 02/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public interface MenuEntryRepository extends CrudRepository<MenuEntry, Long>{
}
