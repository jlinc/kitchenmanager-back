package com.kitchen.repository;

import com.kitchen.model.Menu;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public interface MenuRepository extends CrudRepository<Menu, Long> {
	List<Menu> findAllByKitchenIdOrderByFromDate(Long kitchenId);

	Menu findByIdAndKitchenId(Long id, Long kitchenId);
}
