package com.kitchen.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
public class KitchenConsumer extends KitchenUser {

	@OneToMany(cascade = CascadeType.ALL)
	private List<MenuOrder> menuOrders;

	public List<MenuOrder> getMenuOrders() {
		return menuOrders;
	}

	public void setMenuOrders(List<MenuOrder> menuOrders) {
		this.menuOrders = menuOrders;
	}

}
