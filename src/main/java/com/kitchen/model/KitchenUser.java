package com.kitchen.model;

import javax.persistence.*;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class KitchenUser {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "UserIdGenerator")
	@TableGenerator(table = "SEQUENCES", name = "UserIdGenerator")
	private Long id;
	private String fullName;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
