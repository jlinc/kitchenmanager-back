package com.kitchen.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
public class Menu {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	private Long kitchenId;
	@OneToMany(cascade = CascadeType.ALL)
	private List<MenuEntry> entries;
	private Date fromDate;
	private Date toDate;

	public List<MenuEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<MenuEntry> entries) {
		this.entries = entries;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getKitchenId() {
		return kitchenId;
	}

	public void setKitchenId(Long kitchenId) {
		this.kitchenId = kitchenId;
	}
}
