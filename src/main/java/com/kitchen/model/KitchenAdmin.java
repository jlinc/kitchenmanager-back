package com.kitchen.model;

import com.kitchen.config.Kitchen;

import javax.persistence.*;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
public class KitchenAdmin extends KitchenUser {
	@OneToOne(cascade = CascadeType.ALL)
	private Kitchen kitchen;
	private String email;
	private String phone;

	public Kitchen getKitchen() {
		return kitchen;
	}

	public void setKitchen(Kitchen kitchen) {
		this.kitchen = kitchen;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
