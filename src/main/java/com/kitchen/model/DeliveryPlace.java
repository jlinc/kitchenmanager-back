package com.kitchen.model;

import org.hibernate.annotations.Formula;

import javax.persistence.*;

/**
 * Created by Luis Manuel Sala Espiell on 13/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
public class DeliveryPlace {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	private String name;
	private String address;
	private boolean approved = Boolean.FALSE;
	private Double longitude;
	private Double latitude;
	private Long ocurrences;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Long getOcurrences() {
		return ocurrences;
	}

	public void setOcurrences(Long ocurrences) {
		this.ocurrences = ocurrences;
	}
}
