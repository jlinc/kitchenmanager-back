package com.kitchen.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
public class MenuOrder {


	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	@ManyToOne
	private MenuEntry menuEntry;
	private Date creationDate;
	@ManyToOne
	@JsonIgnore
	private KitchenConsumer kitchenConsumer;
	@ManyToOne
	private MenuEntryFood selectedFood;
	private boolean paid = false;
	private boolean approved = false;
	private boolean delivered = false;
	@ManyToOne
	private DeliveryPlace deliveryPlace;

	public MenuOrder() {
	}

	public MenuEntry getMenuEntry() {
		return menuEntry;
	}

	public void setMenuEntry(MenuEntry menuEntry) {
		this.menuEntry = menuEntry;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public KitchenConsumer getKitchenConsumer() {
		return kitchenConsumer;
	}

	public void setKitchenConsumer(KitchenConsumer kitchenConsumer) {
		this.kitchenConsumer = kitchenConsumer;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public MenuEntryFood getSelectedFood() {
		return selectedFood;
	}

	public void setSelectedFood(MenuEntryFood selectedFood) {
		this.selectedFood = selectedFood;
	}

	public DeliveryPlace getDeliveryPlace() {
		return deliveryPlace;
	}

	public void setDeliveryPlace(DeliveryPlace deliveryPlace) {
		this.deliveryPlace = deliveryPlace;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}
}
