package com.kitchen.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Entity
public class MenuEntry {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	private DayOfWeek dayOfWeek;
	private Date dueDate;
	private boolean disabled;
	@OneToMany(cascade = CascadeType.ALL)
	private List<MenuEntryFood> foods;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public List<MenuEntryFood> getFoods() {
		return foods;
	}

	public void setFoods(List<MenuEntryFood> foods) {
		this.foods = foods;
	}
}
