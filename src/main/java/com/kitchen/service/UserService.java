package com.kitchen.service;

import com.kitchen.controller.request.CreateKitchenConsumerRequest;
import com.kitchen.error.ErrorCode;
import com.kitchen.error.InvalidIdException;
import com.kitchen.error.ResourceNotFoundException;
import com.kitchen.model.KitchenAdmin;
import com.kitchen.model.KitchenConsumer;
import com.kitchen.repository.KitchenAdminRepository;
import com.kitchen.repository.KitchenConsumerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Service
public class UserService {

	@Autowired
	private KitchenAdminRepository adminRepo;
	@Autowired
	private KitchenConsumerRepository kitchenConsumerRepository;

	public KitchenAdmin getAdmin(Long userId) {
		KitchenAdmin admin = adminRepo.findOne(userId);
		if (admin == null) {
			throw new ResourceNotFoundException(String.format("Admin %s not found", userId), ErrorCode.ADMIN_NOT_FOUND);
		}
		return admin;
	}

	public KitchenConsumer getConsumer(Long userId) {
		KitchenConsumer consumer = kitchenConsumerRepository.findOne(userId);
		if (consumer == null) {
			throw new ResourceNotFoundException(String.format("Consumer %s not found", userId), ErrorCode.CONSUMER_NOT_FOUND);
		}
		return consumer;
	}

	public void validateUserKitchen(Long userId, Long kitchenId) {
		KitchenAdmin admin = getAdmin(userId);
		if (admin.getKitchen().getId() == null) {
			throw new ResourceNotFoundException("Admin has no kitchen assigned", ErrorCode.MISSING_KITCHEN);
		}
		if (!admin.getKitchen().getId().equals(kitchenId)){
			throw new InvalidIdException("Admin has a different kitchen assigned", ErrorCode.INVALID_KITCHEN);
		}
	}

	public KitchenAdmin saveAdmin(KitchenAdmin admin) {
		return adminRepo.save(admin);
	}

	public KitchenConsumer saveConsumer(CreateKitchenConsumerRequest consumer) {
		return kitchenConsumerRepository.save(consumer.getUser());
	}
}
