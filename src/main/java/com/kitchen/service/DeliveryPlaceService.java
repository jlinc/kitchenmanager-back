package com.kitchen.service;

import com.google.common.collect.Lists;
import com.kitchen.error.ErrorCode;
import com.kitchen.error.ResourceNotFoundException;
import com.kitchen.model.DeliveryPlace;
import com.kitchen.repository.DeliveryPlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 13/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Service
public class DeliveryPlaceService {

	@Autowired
	private DeliveryPlaceRepository repository;
	@Autowired
	private MenuOrderService orderService;

	public DeliveryPlace getById(Long id) {
		DeliveryPlace one = repository.findOne(id);
		if (one == null) {
			throw new ResourceNotFoundException(String.format("Place %s not found", id), ErrorCode.PLACE_NOT_FOUND);
		}
		Long placeOccurrences = orderService.getPlaceOccurrences(id);
		one.setOcurrences(placeOccurrences);
		return one;
	}

	public DeliveryPlace saveNewPlace(DeliveryPlace place) {
		DeliveryPlace save = repository.save(place);
		return save;
	}

	public List<DeliveryPlace> getAllPlaces(Integer quantity) {
		List<DeliveryPlace> all;
		if (quantity != null) {
			PageRequest pageRequest = new PageRequest(0, quantity);
			all = repository.findAll(pageRequest).getContent();
		}
		else {
			all = Lists.newArrayList(repository.findAll());
		}
		List<DeliveryPlace> deliveryPLaces = Lists.newArrayList(all);
		if (deliveryPLaces == null) {
			return Lists.newArrayList();
		}
		for (DeliveryPlace deliveryPlace : deliveryPLaces) {
			deliveryPlace.setOcurrences(orderService.getPlaceOccurrences(deliveryPlace.getId()));
		}
		Collections.sort(deliveryPLaces, new Comparator<DeliveryPlace>() {
			@Override
			public int compare(DeliveryPlace o1, DeliveryPlace o2) {
				return o2.getOcurrences().compareTo(o1.getOcurrences());
			}
		});
		return deliveryPLaces;
	}

}
