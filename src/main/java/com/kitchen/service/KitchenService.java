package com.kitchen.service;

import com.google.common.collect.Lists;
import com.kitchen.config.Kitchen;
import com.kitchen.error.ErrorCode;
import com.kitchen.error.ResourceNotFoundException;
import com.kitchen.model.KitchenAdmin;
import com.kitchen.repository.KitchenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Service
public class KitchenService {

	@Autowired
	private KitchenRepository kitchenRepository;
	@Autowired
	private UserService userService;

	public List<Kitchen> getAllKitchens() {
		return Lists.newArrayList(kitchenRepository.findAll());
	}

	public Kitchen getKitchen(Long kitchenId) {
		Kitchen savedKitchen = kitchenRepository.findOne(kitchenId);
		if (savedKitchen == null) {
			throw new ResourceNotFoundException(String.format("Kitchen %s not found", kitchenId), ErrorCode.KITCHEN_NOT_FOUND);
		}
		return savedKitchen;
	}

	public Kitchen createKitchen(Kitchen kitchen, Long userId) {
		KitchenAdmin admin = userService.getAdmin(userId);
		if (admin == null) {
			throw new ResourceNotFoundException(String.format("Admin %s not found", userId), ErrorCode.ADMIN_NOT_FOUND);
		}
		Kitchen savedKitchen = kitchenRepository.save(kitchen);
		admin.setKitchen(savedKitchen);
		userService.saveAdmin(admin);
		return savedKitchen;
	}
}
