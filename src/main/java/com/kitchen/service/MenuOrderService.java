package com.kitchen.service;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.kitchen.controller.request.MenuOrderRequest;
import com.kitchen.error.ErrorCode;
import com.kitchen.error.ResourceNotFoundException;
import com.kitchen.model.*;
import com.kitchen.repository.MenuOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 02/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Service
public class MenuOrderService {

	@Autowired
	private MenuOrderRepository menuOrderRepository;
	@Autowired
	private MenuService menuService;
	@Autowired
	private UserService userService;
	@Autowired
	private DeliveryPlaceService deliveryPlaceService;

	public MenuOrder placeOrder(Long userId, final MenuOrderRequest request) {
		MenuEntry menuById = menuService.getMenuEntry(request.getKitchenId(), request.getMenuId(), request.getEntryId());
		KitchenConsumer consumer = userService.getConsumer(userId);
		MenuOrder order = new MenuOrder();
		order.setCreationDate(Calendar.getInstance().getTime());
		order.setKitchenConsumer(consumer);
		order.setMenuEntry(menuById);
		Optional<MenuEntryFood> foodOption = Iterables.tryFind(menuById.getFoods(), new Predicate<MenuEntryFood>() {
			@Override
			public boolean apply(MenuEntryFood menuEntryFood) {
				return menuEntryFood.getId().equals(request.getFoodId());
			}
		});
		if (!foodOption.isPresent()) {
			throw new ResourceNotFoundException(String.format("Could not find food with id %s", request.getFoodId()),
					ErrorCode.MISSING_FOOD);
		}
		order.setDeliveryPlace(deliveryPlaceService.getById(request.getPlaceId()));
		order.setSelectedFood(foodOption.get());
		return menuOrderRepository.save(order);
	}

	public List<MenuOrder> getUserOrders(Long userId) {
		KitchenConsumer consumer = userService.getConsumer(userId);
		//TODO Luis usar exists de bd
		return menuOrderRepository.findAllByKitchenConsumerId(userId);
	}

	public Long getPlaceOccurrences(Long id) {
		return menuOrderRepository.countByDeliveryPlaceId(id);
	}

}
