package com.kitchen.service;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.kitchen.error.ErrorCode;
import com.kitchen.error.ResourceNotFoundException;
import com.kitchen.model.KitchenAdmin;
import com.kitchen.model.Menu;
import com.kitchen.model.MenuEntry;
import com.kitchen.repository.MenuEntryRepository;
import com.kitchen.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@Service
public class MenuService {

	@Autowired
	private MenuRepository repository;
	@Autowired
	private MenuEntryRepository menuEntryRepository;
	@Autowired
	private KitchenService kitchenService;
	@Autowired
	private UserService userService;

	public Menu saveMenu(Long kitchenId, Menu menu, Long userId) {
		userService.validateUserKitchen(userId, kitchenId);
		menu.setKitchenId(kitchenId);
		Menu saved = repository.save(menu);
		return saved;
	}

	public List<Menu> getAllMenues(Long kitchenId) {
		//checking the kitchen exists
		kitchenService.getKitchen(kitchenId);
		return Lists.newArrayList(repository.findAllByKitchenIdOrderByFromDate(kitchenId));
	}

	public Menu getMenuById(Long kitchenId, Long menuId) {
		Menu byIdAndKitchenId = repository.findByIdAndKitchenId(menuId, kitchenId);
		if (byIdAndKitchenId == null) {
			throw new ResourceNotFoundException(String.format("Menu %s was not found on kitchen %s", menuId,
					kitchenId), ErrorCode.MISSING_MENU);
		}
		return byIdAndKitchenId;
	}

	public MenuEntry getMenuEntry(Long kitchenId, Long menuId, final Long menuEntryId) {
		Menu menuById = this.getMenuById(kitchenId, menuId);
		Optional<MenuEntry> menuEntryOptional = Iterables.tryFind(menuById.getEntries(), new Predicate<MenuEntry>() {
			@Override
			public boolean apply(MenuEntry menuEntry) {
				return menuEntry.getId().equals(menuEntryId);
			}
		});
		if (!menuEntryOptional.isPresent()) {
			throw new ResourceNotFoundException(String.format("Menu entry %s was not found", menuEntryId),
					ErrorCode.MISSING_MENU_ENTRY);
		}
		return menuEntryOptional.get();
	}
}
