package com.kitchen.controller;

import com.kitchen.controller.request.CreateKitchenConsumerRequest;
import com.kitchen.model.KitchenAdmin;
import com.kitchen.model.KitchenConsumer;
import com.kitchen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users/admin" ,method = RequestMethod.POST)
	public KitchenAdmin createUserAdmin(@RequestBody KitchenAdmin admin){
		return userService.saveAdmin(admin);
	}

	@RequestMapping(value = "/users/consumer" ,method = RequestMethod.POST)
	public KitchenConsumer createUserConsumer(@RequestBody CreateKitchenConsumerRequest consumer){
		return userService.saveConsumer(consumer);
	}

}
