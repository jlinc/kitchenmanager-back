package com.kitchen.controller;

import com.kitchen.controller.request.MenuOrderRequest;
import com.kitchen.model.MenuOrder;
import com.kitchen.service.MenuOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@RestController
public class OrderController {

	@Autowired
	private MenuOrderService menuOrderService;

	@RequestMapping(value = "users/{userId}/orders", method = RequestMethod.POST)
	public MenuOrder placeAnOrder(@PathVariable Long userId, @RequestBody MenuOrderRequest request){
		return menuOrderService.placeOrder(userId, request);
	}

	@RequestMapping(value = "users/{userId}/orders", method = RequestMethod.GET)
	public List<MenuOrder> getUserOrders(@PathVariable Long userId){
		return menuOrderService.getUserOrders(userId);
	}

}
