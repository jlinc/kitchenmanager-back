package com.kitchen.controller;

import com.kitchen.model.Menu;
import com.kitchen.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@RestController
public class MenuController {

	@Autowired
	private MenuService menuService;

	@RequestMapping(value = "/users/{userId}/kitchens/{kitchenId}/menues", method = RequestMethod.POST)
	public Menu createNewMenu(@RequestBody Menu menu, @PathVariable Long kitchenId, @PathVariable Long userId) {
		Menu savedMenu= menuService.saveMenu(kitchenId, menu, userId);
		return savedMenu;
	}

	@RequestMapping(value="/kitchens/{kitchenId}/menues", method = RequestMethod.GET)
	public List<Menu> getAllMenues(@PathVariable Long kitchenId){
		return menuService.getAllMenues(kitchenId);
	}


	@RequestMapping(value="/kitchens/{kitchenId}/menues/{menuId}", method = RequestMethod.GET)
	public Menu getMenu(@PathVariable Long kitchenId, @PathVariable Long menuId){
		return menuService.getMenuById(kitchenId, menuId);
	}


}
