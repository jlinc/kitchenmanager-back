package com.kitchen.controller;

import com.kitchen.config.Kitchen;
import com.kitchen.service.KitchenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 30/04/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@RestController
public class KitchenController {

	@Autowired
	private KitchenService kitchenService;

	@RequestMapping(value = "/kitchens", method = RequestMethod.GET)
	public List<Kitchen> getAllKitchens() {
		return kitchenService.getAllKitchens();
	}

	@RequestMapping(value = "/kitchens/{kitchenId}", method = RequestMethod.GET)
	public Kitchen getKitchen(@PathVariable Long kitchenId) {
		return kitchenService.getKitchen(kitchenId);
	}

	@RequestMapping(value = "/users/{userId}/kitchens", method = RequestMethod.POST)
	public Kitchen createKitchen(@RequestBody Kitchen kitchen, @PathVariable Long userId) {
		return kitchenService.createKitchen(kitchen, userId);
	}

}
