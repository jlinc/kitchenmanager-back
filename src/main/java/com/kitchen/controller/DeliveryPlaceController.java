package com.kitchen.controller;

import com.kitchen.model.DeliveryPlace;
import com.kitchen.service.DeliveryPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Luis Manuel Sala Espiell on 13/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
@RestController
public class DeliveryPlaceController {

	@Autowired
	private DeliveryPlaceService deliveryPlaceService;

	@RequestMapping(value = "/places", method = RequestMethod.POST)
	public DeliveryPlace createNewPlace(@RequestBody DeliveryPlace place){
		return deliveryPlaceService.saveNewPlace(place);
	}

	@RequestMapping(value = "/places/{placeId}", method = RequestMethod.GET)
	public DeliveryPlace createNewPlace(@PathVariable Long placeId){
		return deliveryPlaceService.getById(placeId);
	}

	@RequestMapping(value = "/places", method = RequestMethod.GET)
	public List<DeliveryPlace> getAllPlaces(@RequestParam(required = false) Integer quantity){
		return deliveryPlaceService.getAllPlaces(quantity);
	}


}
