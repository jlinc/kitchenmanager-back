package com.kitchen.controller.request;

import com.kitchen.model.KitchenConsumer;

/**
 * Created by Luis Manuel Sala Espiell on 13/05/16.
 *
 * @author <a href="mailto:salaesp@gmail.com">Luis Manuel Sala Espiell</a>
 */
public class CreateKitchenConsumerRequest {
	private String fullName;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public KitchenConsumer getUser() {
		KitchenConsumer kitchenConsumer = new KitchenConsumer();
		kitchenConsumer.setFullName(this.fullName);
		return kitchenConsumer;
	}
}
